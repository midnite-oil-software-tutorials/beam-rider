using UnityEngine;

public class KillOnTouch : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.TryGetComponent<Killable>(out var target)) return;
        target.KillMe();
        Destroy(gameObject);
    }
}
