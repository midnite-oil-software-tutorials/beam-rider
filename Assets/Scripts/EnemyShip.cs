using System;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyShip : MonoBehaviour, IHitBarrier
{
    [SerializeField] Vector2 _spawnPosition = new Vector2(8f, 3.2f);
    [SerializeField] float _baseMoveSpeed = 3f, _attackDelayMin = 1f, _attackDelayMax = 3f, _maxScale = 1.5f;
    [SerializeField] float _fireDelayMin = 1f, _fireDelayMax = 5f;
    [SerializeField] AudioClip _fireSound;
    [SerializeField] Projectile _projectilePrefab;
    [SerializeField] Transform _gun;
    
    Transform _transform;
    EnemyState _state;
    int _direction = 1;
    float _attackTime, _fireTime;

    bool ShouldAttack => Time.time >= _attackTime;
    bool ShouldReverseDirection
    {
        get
        {
            if (_direction > 0 && _transform.position.x >= _spawnPosition.x)
            {
                return true;
            }

            return (_direction < 0 && _transform.position.x <= _spawnPosition.x * -1);
        }
    }

    float MoveSpeed => _baseMoveSpeed + (0.5f * (_spawnPosition.y - _transform.position.y));

    float Scale => Mathf.Min(0.25f + (_spawnPosition.y - _transform.position.y), _maxScale);
    bool ShouldFire => Time.time >= _fireTime;

    enum EnemyState
    {
        Idle,
        Attacking
    }

    void Awake()
    {
        _transform = transform;
    }

    void Start()
    {
        GameManager.Instance.GameStateChanged += OnGameStateChanged;
        EnterIdleState();
    }

    void OnDestroy()
    {
        GameManager.Instance.GameStateChanged -= OnGameStateChanged;
    }

    void OnGameStateChanged(GameState gameState)
    {
        if (gameState == GameState.GameOver)
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (!GameManager.Instance.IsPlaying) return;
        switch (_state)
        {
            case EnemyState.Idle:
                HandleIdle();
                break;
            case EnemyState.Attacking:
                HandleAttacking();
                break;
        }
    }
    
    void EnterAttackState()
    {
        _state = EnemyState.Attacking;
        if (Mathf.Approximately(0f, _fireTime))
        {
            SetFireTime();
        }
    }

    void EnterIdleState()
    {
        _state = EnemyState.Idle;
        _transform.position = new Vector3(Random.Range(_spawnPosition.x * -1, _spawnPosition.x), _spawnPosition.y, -1f);
        _transform.localScale = new Vector3(0.25f, 0.25f, 1f);
        _attackTime = Time.time + Random.Range(_attackDelayMin, _attackDelayMax);
    }

    void HandleIdle()
    {
        if (ShouldAttack)
        {
            EnterAttackState();
            return;
        }
        if (ShouldReverseDirection)
        {
            _direction *= -1;
        }

        var position = _transform.position;
        position.x += _direction * (MoveSpeed * Time.deltaTime);
        _transform.position = position;
    }

    void HandleAttacking()
    {
        _transform.position += Vector3.down * MoveSpeed * Time.deltaTime;
        var scale = _transform.localScale;
        scale.x = scale.y = Scale;
        _transform.localScale = scale;
        if (ShouldFire)
        {
            FireProjectile();
        }
    }

    void FireProjectile()
    {
        SetFireTime();
        SoundManager.Instance.PlayAudioClip(_fireSound);
        Instantiate(_projectilePrefab, _gun.position, quaternion.identity);
    }

    void SetFireTime()
    {
        _fireTime = Time.time + Random.Range(_fireDelayMin, _fireDelayMax);
    }

    public void HitBarrier()
    {
        EnterIdleState();
    }
}