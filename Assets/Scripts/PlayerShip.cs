using UnityEngine;

public class PlayerShip : MonoBehaviour
{
    [SerializeField] float _moveSpeed = 10f, _moveLimit = 8f, _fireDelay = 0.25f;
    [SerializeField] Projectile _projectilePrefab, _torpedoPrefab;
    [SerializeField] AudioClip _fireSound, _missileSound;
    [SerializeField] Transform _gun;
    
    Transform _transform;
    float _fireTime;
    float _torpedoFireTime;
    int Torpedoes => GameManager.Instance.Torpedoes;
    bool CanFire => Time.time >= _fireTime;
    bool CanFireTorpedo => (Torpedoes > 0 && (Time.time >= _torpedoFireTime));

    void Awake()
    {
        _transform = transform;
    }

    void Start()
    {
        UserInput.Instance.OnFirePressed += OnFirePressed;
        UserInput.Instance.OnTorpedoPressed += OnTorpedoPressed;
    }

    void OnDestroy()
    {
        UserInput.Instance.OnFirePressed -= OnFirePressed;
        UserInput.Instance.OnTorpedoPressed -= OnTorpedoPressed;
    }

    void Update()
    {
        if (UserInput.Instance.MoveInput.x > 0f && _transform.position.x < _moveLimit)
        {
            _transform.position += Vector3.right * (_moveSpeed * Time.deltaTime);
            return;
        }

        if (UserInput.Instance.MoveInput.x < 0f && _transform.position.x > -_moveLimit)
        {
            _transform.position -= Vector3.right * (_moveSpeed * Time.deltaTime);
        }
    }

    void OnFirePressed()
    {
        if (CanFire)
        {
            FireProjectile();
        }
    }

    void OnTorpedoPressed()
    {
        if (CanFireTorpedo)
        {
            FireTorpedo();
        }
    }

    void FireProjectile()
    {
        _fireTime = Time.time + _fireDelay;
        if (_fireSound)
        {
            SoundManager.Instance.PlayAudioClip(_fireSound);
        }
        Instantiate(_projectilePrefab, _gun.position, Quaternion.identity);
    }

    void FireTorpedo()
    {
        _torpedoFireTime = Time.time + _fireDelay;
        GameManager.Instance.FireTorpedo();
        if (_missileSound)
        {
            SoundManager.Instance.PlayAudioClip(_missileSound);
        }
        Instantiate(_torpedoPrefab, _gun.position, Quaternion.identity);
    }
}
