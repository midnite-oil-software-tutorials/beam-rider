using System.Collections;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] AudioClip _audioClip;
    
    Animator _animator;

    bool AnimationFinished
    {
        get
        {
            if (!_animator) return true;
            var stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
            return stateInfo.normalizedTime >= 1.0f;
        }
    }

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    IEnumerator Start()
    {
        if (_audioClip)
        {
            SoundManager.Instance.PlayAudioClip(_audioClip);
        }
        while (!AnimationFinished) yield return null;
        Destroy(gameObject);
    }
}
