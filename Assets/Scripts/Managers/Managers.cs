public class Managers : SingletonMonoBehaviour<Managers>
{
    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }
}
