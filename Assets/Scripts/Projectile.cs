using UnityEngine;

public class Projectile : MonoBehaviour, IHitBarrier
{
    [SerializeField] float _movementSpeed = 10f;
    Transform _transform;
    Vector3 _scaleAdjust;

    void Awake()
    {
        _transform = transform;
        _scaleAdjust = new Vector3(0.05f, 0.05f, 0);
    }

    void Update()
    {
        _transform.position += Vector3.up * (_movementSpeed * Time.deltaTime);
        _transform.localScale -= _scaleAdjust * Time.deltaTime;
    }

    public void HitBarrier()
    {
        Destroy(gameObject);
    }
}
