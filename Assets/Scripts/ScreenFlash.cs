using UnityEngine;

public class ScreenFlash : MonoBehaviour
{
    [SerializeField] SpriteRenderer _renderer;
    [SerializeField] Color _startColor, _endColor;
    [SerializeField] float _fadeTime = 0.5f;

    float _currentTime;
    
    void OnEnable()
    {
        _renderer.material.color = _startColor;
        _currentTime = 0f;
    }

    void Update()
    {
        _currentTime = Mathf.Min(_currentTime + Time.deltaTime, _fadeTime);
        var lerpAmount = _currentTime / _fadeTime;
        _renderer.material.color = Color.Lerp(_startColor, _endColor, lerpAmount);
        if (_currentTime >= _fadeTime)
        {
            Destroy(gameObject);
        }
    }
    
}
