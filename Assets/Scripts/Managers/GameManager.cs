using System;
using System.Collections;
using UnityEngine;

public class GameManager : SingletonMonoBehaviour<GameManager>
{
    public event Action<GameState> GameStateChanged = delegate(GameState state) {  };
    public event Action<int> PlayerLivesChanged = delegate(int playerLives) {  };
    public event Action<int> ScoreChanged = delegate(int score) {  };
    public event Action<int> TorpedoesUpdated = delegate(int torpedoesRemaining) {  };
    public event Action<int> EnemiesUpdated = delegate(int enemies) {  };
    public event Action<int> SectorUpdated = delegate(int sector) {  };

    [SerializeField] GameObject _startScreen, _gameOverScreen;
    [SerializeField] PlayerShip _playerShipPrefab;
    WaitForSeconds _spawnPlayerDelay;

    public void EntityDied(Killable entity)
    {
        HandleEnemyDeath(entity);

        HandleBossDeath(entity);

        HandlePlayerDeath(entity);
    }

    public void FireTorpedo()
    {
        UpdateTorpedoes(Torpedoes - 1);
    }

    public GameState GameState { get; private set; }
    public bool IsPlaying => GameState == GameState.Playing;
    public int PlayerLives { get; private set; }

    public int Score { get; private set; }

    public int Torpedoes { get; private set; }

    public int EnemiesUntilBoss { get; private set; }

    public int Sector { get; private set; }

    public void ResetScore()
    {
        Score = 0;
        ScoreChanged(Score);
    }

    public void AddPoints(int points)
    {
        Score += points;
        ScoreChanged(Score);
    }

    public void NextSector()
    {
        ++Sector;
        EnemiesUntilBoss = 8 + (2 * Sector);
        Torpedoes = 3;
        UpdateEnemies(EnemiesUntilBoss);
        UpdateSector(Sector);
        UpdateTorpedoes(Torpedoes);
    }

    public void StartGame()
    {
        ResetScore();
        UpdateTorpedoes(3);
        UpdatePlayerLives(3);
        UpdateEnemies(12);
        UpdateSector(1);
        SpawnPlayerShip();
        SetGameState(GameState.Playing);
    }

    void Start()
    {
        _spawnPlayerDelay = new WaitForSeconds(3f);
    }

    void OnEnable()
    {
        _startScreen.SetActive(true);
        SetGameState(GameState.WaitingToStart);
    }

    void SetGameState(GameState state)
    {
        GameState = state;
        GameStateChanged(GameState);
    }

    void HandleBossDeath(Killable entity)
    {
        if (entity.gameObject.TryGetComponent<Boss>(out var boss))
        {
            NextSector();
        }
    }

    void HandleEnemyDeath(Killable entity)
    {
        if (entity.gameObject.TryGetComponent<EnemyShip>(out var enemy))
        {
            UpdateEnemies(Math.Max(EnemiesUntilBoss - 1, 0));
        }
    }

    void HandlePlayerDeath(Killable entity)
    {
        if (!entity.gameObject.TryGetComponent<PlayerShip>(out var player)) return;
        --PlayerLives;
        UpdatePlayerLives(PlayerLives);
        if (PlayerLives < 1)
        {
            GameOver();
            return;
        }

        StartCoroutine(SpawnPlayerShipAfterDelay());
    }

    IEnumerator SpawnPlayerShipAfterDelay()
    {
        yield return _spawnPlayerDelay;
        SpawnPlayerShip();
    }

    void GameOver()
    {
        SetGameState(GameState.GameOver);
        _gameOverScreen.SetActive(true);
    }

    void SpawnPlayerShip()
    {
        Instantiate(_playerShipPrefab);
    }

    void UpdatePlayerLives(int playerLives)
    {
        PlayerLives = playerLives;
        PlayerLivesChanged(PlayerLives);
    }

    void UpdateTorpedoes(int torpedoes)
    {
        Torpedoes = torpedoes;
        TorpedoesUpdated(torpedoes);
    }

    void UpdateEnemies(int enemies)
    {
        EnemiesUntilBoss = enemies;
        EnemiesUpdated(EnemiesUntilBoss);
    }

    void UpdateSector(int sector)
    {
        Sector = sector;
        SectorUpdated(Sector);
    }
}