using UnityEngine;

public class BeamSpawner : MonoBehaviour
{
    [SerializeField] GameObject _beamPrefab;
    [SerializeField] float _spawnInterval = 1f;

    float _nextSpawnTime;

    bool ShouldSpawnBeam() => GameManager.Instance.IsPlaying && (Time.time >= _nextSpawnTime);

    void Start()
    {
        GameManager.Instance.GameStateChanged += OnGameStateChanged;
    }

    void OnGameStateChanged(GameState gameState)
    {
        if (gameState == GameState.Playing)
        {
            StartSpawning();
        }
    }

    void Update()
    {
        if (ShouldSpawnBeam())
        {
            SpawnBeam();
        }
    }

    void StartSpawning()
    {
        _nextSpawnTime = Time.time + -_spawnInterval;
    }

    void SpawnBeam()
    {
        _nextSpawnTime = Time.time + _spawnInterval;
        Instantiate(_beamPrefab, transform);
    }
}
