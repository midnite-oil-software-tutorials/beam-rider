using UnityEngine;

public class Killable : MonoBehaviour
{
    [SerializeField] ScreenFlash _screenFlashPrefab;
    [SerializeField] Explosion _explosionPrefab;
    [SerializeField] int _points = 100;

    public void KillMe()
    {
        FlashScreen();
        ShowExplosion();
        GameManager.Instance.AddPoints(_points);
        GameManager.Instance.EntityDied(this);
        Destroy(gameObject);
    }

    void ShowExplosion()
    {
        if (_explosionPrefab)
        {
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
        }
    }

    void FlashScreen()
    {
        if (_screenFlashPrefab)
        {
            Instantiate(_screenFlashPrefab);
        }
    }
}
