using System;
using UnityEngine;
using UnityEngine.InputSystem;

public interface IUserInput
{
    event Action<InputValue> OnMoveReceived;
    event Action OnFirePressed;
    event Action OnTorpedoPressed;
    Vector2 MoveInput { get; }
}